require 'gemojione'

module Emoji
  extend self

  def emojis
    Gemojione.index.instance_variable_get(:@emoji_by_name)
  end

  def emojis_names
    emojis.keys.sort
  end

  def emoji_filename(name)
    emojis[name]["unicode"]
  end

  def images_path
    File.expand_path("../../assets/images", __FILE__)
  end
end
