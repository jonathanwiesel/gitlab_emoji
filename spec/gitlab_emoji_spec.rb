require 'spec_helper'
require_relative '../lib/gitlab_emoji'

describe Emoji do
  describe 'emojis' do
    let(:emojis) { Emoji.emojis }

    it { emojis.should be_a(Hash) }
    it { emojis.size.should eq(1267) }
  end

  describe 'emojis_names' do
    subject { Emoji.emojis_names }

    it { should be_a(Array) }
    it { should include ("+1") }
  end

  describe 'emoji_filename' do
    subject { Emoji.emoji_filename("+1") }

    it { should eq("1F44D") }
  end
end
